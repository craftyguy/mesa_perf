#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from testers import PerfTester


class HeavenTimeout:

    def GetDuration(self):
        return 45


# TODO: run 'windowed' since systems no longer have hdmi dummy adapters and
# 'fullscreen' fails
build(PerfTester("unigine.heaven", iterations=3, windowed=True,
                  env={"allow_glsl_extension_directive_midshader":"true",
                       "dual_color_blend_by_location":"true"}),
      time_limit=HeavenTimeout())
