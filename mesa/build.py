#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from builders import MesonBuilder
from export import Export
from project_invoke import ProjectInvoke

def main():
    options = [
        '-Dgallium-drivers=iris',
        '-Ddri-drivers=',
        '-Dvulkan-drivers=intel',
        # vk overlay needed for measuring some games
        '-Dvulkan-overlay-layer=true',
        '-Dtools=intel',
        '-Dbuildtype=release',
        '-Db_ndebug=true',
        '-Dllvm=false',
        '-Dprefer-iris=false',
        '-Dvalgrind=false',
    ]

    minor_version = tuple(ProjectInvoke().get_mesa_version().quad[0:2])
    cmp_version = (20, 3)
    platform_options = '-Dplatforms=x11'
    if minor_version < cmp_version:
        platform_options = ','.join([platform_options, 'drm'])
    options += [platform_options]

    b = MesonBuilder(extra_definitions=options, install=True)
    build(b)

    Export().export_perf()


if __name__ == '__main__':
    main()
