#!/usr/bin/env python3
import sys
import os
import os.path as path
sys.path.append(path.join(path.dirname(path.abspath(sys.argv[0])), "..",
                          "repos", "mesa_ci", "build_support"))
from build_support import build
from builders import AbnBuilder
from options import Options

ABN_VERSION = '2.2.220'


def main():
    # When run with build_local, the override var is in os.environ under the
    # var's name. When run in CI, the override var is in the 'env' variable in
    # os.environ. This handles both cases.
    env = os.environ.get('env')
    if env:
        for var in env.split():
            if 'ABN_VERSION' in var:
                abn_version = var.split('=')[1]
                break
    else:
        abn_version = os.environ.get('ABN_VERSION')
    if not abn_version:
        abn_version = ABN_VERSION
    b = AbnBuilder(buildnum=abn_version)
    build(b)


if __name__ == '__main__':
    main()
