#!/bin/bash

function usage_exit() {
        echo "Usage: $0 <tester hostname>"
        exit 1
}

[ -z "$1" ] && usage_exit

host="$1"
[[ "$host" != *"mesa-perf"* ]] && host="mesa-perf-$host.local"

ssh -L 5900:localhost:5900 -J otc-mesa-android.jf.intel.com "jenkins@$host" 'x11vnc -localhost -display :0' &
sleep 5
vncviewer -PreferredEncoding=ZRLE localhost:0
