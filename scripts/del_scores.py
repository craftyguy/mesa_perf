#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2020.  All Rights Reserved.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
import argparse
import glob
import os
import shutil
import sys
from pony.orm import db_session
from pony import orm
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "../", "repos", "mesa_perf_results"))
from models import db
# Initialize db object
sql_user = 'jenkins'
if "SQL_DATABASE_USER" in os.environ:
    sql_user = os.environ["SQL_DATABASE_USER"]

sql_pw = ''
if "SQL_DATABASE_PW_FILE" in os.environ:
    sql_pw_file = os.environ["SQL_DATABASE_PW_FILE"]
    if os.path.exists(sql_pw_file):
        with open(sql_pw_file, 'r') as f:
            sql_pw = f.read().rstrip()
sql_host = "localhost"
if "SQL_DATABASE_HOST" in os.environ:
    sql_host = os.environ["SQL_DATABASE_HOST"]

if sql_pw:
    db.bind('mysql', host=sql_host, user=sql_user, password=sql_pw,
            database='mesa_perf_results')
else:
    db.bind('mysql', host=sql_host, user=sql_user,
            database='mesa_perf_results')
db.generate_mapping(create_tables=True)


@db_session
def main():
    parser = argparse.ArgumentParser(description=("Remove scores for a given "
                                                  "platform and/or benchmark"))
    parser.add_argument('-b', '--benchmark', type=str, default="",
                        help=("Benchmark to remove scores. Required if no "
                              "other options specified"))
    parser.add_argument('-p', '--platform', type=str, default="",
                        help=("Platform to remove scores. Required if no "
                              "other options specified"))
    opts = parser.parse_args()

    if not opts.benchmark and not opts.platform:
        parser.print_help()
        sys.exit(1)

    benchmark = opts.benchmark if opts.benchmark else '*'
    scores_to_del = glob.glob('/mnt/jenkins/results/mesa_master/*/*/*/scores/'
                              + benchmark + '/' + opts.platform)
    buildinfo_to_del = glob.glob('/mnt/jenkins/results/mesa_master/*/*/'
                                 + benchmark + '/*/*/' + opts.platform)
    for score in scores_to_del:
        shutil.rmtree(score)
    print('scores removed from /mnt/jenkins/results/mesa_master/*/*/*/scores/'
          + benchmark + '/' + opts.platform)
    for info in buildinfo_to_del:
        shutil.rmtree(info)
    print('build info removed from /mnt/jenkins/results/mesa_master/*/*/'
          + benchmark + '/*/*/' + opts.platform)

    job = orm.select(j for j in db.Job if j.name == 'mesa_master').limit(1)
    if not job:
        print("no builds in job")
        return
    job = job[0]
    if opts.benchmarks and opts.platform:
        orm.delete(s for s in db.Score
                   if (s.benchmark.name == opts.benchmark
                       and s.hardware.hardware == opts.platform
                       and s.job.id == job.id))
        db.commit()
        print('scores removed from database for ' + opts.platform
              + 'benchmark: ' + opts.benchmark)
    elif opts.benchmark:
        orm.delete(s for s in db.Score
                   if (s.benchmark.name == opts.benchmark
                       and s.job.id == job.id))
        db.commit()
        print('scores removed from database for benchmark: ' + opts.benchmark)
    elif opts.platform:
        orm.delete(s for s in db.Score
                   if (s.hardware.hardware == opts.platform
                       and s.job.id == job.id))
        db.commit()
        print('scores removed from database for platform: ' + opts.platform)


if __name__ == "__main__":
    main()
