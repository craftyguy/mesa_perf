#!/usr/bin/env python3

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from testers import PerfTester
from options import Options


class SynmarkTimeout:
    def __init__(self):
        self._options = Options()
    def GetDuration(self):
        limit = 30
        if self._options.type == "daily":
            limit *= 8
        return limit

def iterations(bench, hw):
    if bench == "synmark.OglBatch6":
        if hw.startswith("gen8"):
            return 7
        return 4
    if bench == "synmark.OglTerrainFlyInst":
        if hw.startswith("gen9"):
            return 3
    if bench == "synmark.OglTerrainFlyTess":
        if hw.startswith("gen8"):
            return 4
    
    
high_variance_benchmarks = ["synmark.OglVSInstancing",
                            "synmark.OglTerrainFlyInst",
                            "synmark.OglCSDof",
                            "synmark.OglBatch6",
                            "synmark.OglTerrainFlyTess",
                            "synmark.OglTerrainPanInst"]

build(PerfTester(high_variance_benchmarks, iterations=2,
                  custom_iterations_fn=iterations),
         time_limit=SynmarkTimeout())

